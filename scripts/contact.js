var form1 = document.querySelector('#contact-form1');

form1.addEventListener('submit', function(event){
    event.preventDefault();
    Swal.fire(
        '¡Enviado correctamente!',
        '¡Gracias por confiar en nosotros, nos pondremos en contacto a la brevedad!',
        'success'
        )
        let formData = new FormData(form1);
        form1.reset();
        console.log(formData)
        fetch("contact.php",
        {
            body: formData,
            method: "post"
        }
    );
});