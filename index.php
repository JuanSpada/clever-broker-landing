<?php require_once('data/localidades.php')?>

<!doctype html>
<html lang="en">
  <head>
  <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-W4QSH8Q');</script>
    <!-- End Google Tag Manager -->
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="images/clever-favicon.png" size="32x32" type="image/png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    
    <!-- My CSS -->
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/responsive.css">
    
    <title>Clever Broker</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W4QSH8Q"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
    <div class="row logo">
        <div class="container">
            <img src="images/logo-clever.png" alt="" size="32x32">
        </div>
    </div>
    <section class="header">
        <div class="automobile-banner-layer" style="background-image: url('images/header.jpg'); background-size: cover; ">
            <img src="images/header.jpg" alt="">
            <div class="automobile-banner-caption">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-xl-6">
                            <div class="automobile-banner-text automobile-banner-text3">
                                <span>Tu Seguro </span>
                                <h1>rápido y fácil</h1>
                                <ul>
                                    <li><i class="fas fa-check-circle"></i>100% Digital</li>
                                    <li><i class="fas fa-check-circle"></i>Contamos con las mejores compañías</li>
                                    <li><i class="fas fa-check-circle"></i>Asesoramiento inmediato por WhatsApp</li>
                                </ul>
                                <!-- <a href="#nosotros" class="automobile-banner-btn automobile-bgcolor">Más Info</a> -->
                            </div>
                        </div>
                        <div class="col-12 col-xl-6">
                            <div class="automobile-banner-tabs">
                                <!-- Nav tabs -->
                                <ul class="nav-tabs" role="tablist">
                                    <!-- <li role="presentation" class="active"><a href="#bannerauto" aria-controls="bannerauto" role="tab" data-toggle="tab">COTIZA EN EL MOMENTO</a></li> -->
                                </ul>
                                <P class="cotiza"><b>ECONOMIZA TU SEGURO!</b></P>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="bannerauto">
                                        <div class="automobile-banner-form">
                                        <?php include ('partials/formcar.php'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
            

    <section class="body" style="padding: 30px 12px">
        <div class="container">
            <div class="row">
                <div class="body-content">
                    <h2>Quienes <span>Somos</span>?</h2>
                    <span>Servicio y Confianza</span>
                    <p>Somos un equipo de asesores de seguros, con años en el mercado y venimos a hacer tu vida mas fácil. Queremos acompañarte durante todo el camino para que tu experiencia sea la mejor de principio a fin. Tu satisfacción es nuestra prioridad.</p>
                    <br>
                    <h2 style="font-size: 18px">Por que <span>elegirnos</span></h2>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row quienes-somos">
                <div class="col-12 col-md-6">
                    <div class="automobile-about-services-list">
                        <ul>
                            <li>
                                <i style="color:#6BAC40" class="fas fa-donate"></i>
                                <div class="automobile-about-services-text">
                                    <h4>Descuentos</h4>
                                    <p>Podés ahorrar hasta un 30% de descuento pagando con débito automático y un 15% adicional cada año que renueves con nosotros.</p>
                                </div>
                            </li>
                            <li>
                                <i style="color:#6BAC40;" class="fas fa-user-friends"></i>
                                <div class="automobile-about-services-text">
                                    <h4>Atención Personalizada</h4>
                                    <p>Queremos que tengas la mejor experiencia  por eso te dedicamos el tiempo que te mereces.</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="automobile-about-services-list">
                        <ul>
                            <li>
                                <i style="color:#6BAC40" class="fas fa-handshake"></i>
                                <div class="automobile-about-services-text">
                                    <h4>Confianza</h4>
                                    <p>Brindamos una experiencia clara y transparente, sin letra chica, por eso trabajamos con las mejores aseguradoras.</p>
                                </div>
                            </li>
                            <li>
                                <i style="color:#6BAC40;" class="fab fa-envira"></i>
                                <div class="automobile-about-services-text">
                                    <h4>100% Digital</h4>
                                    <p id="companias">Nos preocupamos por el medio ambiente, por eso trabajamos con un proceso 100% digital evitando el uso innecesario de papel.</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </section>
    
    <?php require_once('partials/footer.php') ?>

    <!-- JQUERY -->
    <script type="text/javascript" src="scripts/jquery.min.js"></script>
    <script type="text/javascript" src="scripts/jquery.countdown.min.js"></script>

    <!-- Dynamic Fields Forms     -->
    <script type="text/javascript" src="scripts/dynamic-fields-cars.js"></script>
    <script type="text/javascript" src="scripts/dynamic-fields-bikes.js"></script>

    <script type="text/javascript" src="scripts/bootstrap.min.js"></script>
    <!-- Sweet Alert -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <!-- Optional JavaScript -->
    <script src="https://kit.fontawesome.com/56081eaeec.js" crossorigin="anonymous"></script>
    <!-- Contact from script -->
    <script src="scripts/contact.js"></script>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <a href="https://wa.me/+5491161847991" class="whatsapp" target="_blank"> <i class="fab fa-whatsapp whatsapp-icon"></i></a> -->
    <!-- Start of HubSpot Embed Code --> <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/7771422.js"></script> <!-- End of HubSpot Embed Code -->
    <!-- Start of HubSpot Embed Code -->
    <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/7771422.js"></script>
    <!-- End of HubSpot Embed Code -->
  </body>
</html>