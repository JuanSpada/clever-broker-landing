<?php
$url = "https://hooks.zapier.com/hooks/catch/803715/oiekhvk/";

$nombre = $_POST['nombre'] ? $_POST['nombre'] : "";
$email = $_POST['email'] ? $_POST['email'] : "";
$fechanacimiento = $_POST['fechanacimiento'] ? $_POST['fechanacimiento'] : "";
$telefono = $_POST['telefono'] ? $_POST['telefono'] : "";
$marca_auto = $_POST['marca_auto'] ? $_POST['marca_auto'] : "";
$modelo_auto = $_POST['modelo_auto'] ? $_POST['modelo_auto'] : "";
$año = $_POST['año'] ? $_POST['año'] : "";
$localidad = $_POST['localidad'] ? $_POST['localidad'] : "";
$utm_source = $_POST['utm_source'] ? $_POST['utm_source'] : "Orgánico";
$utm_medium = $_POST['utm_medium'] ? $_POST['utm_medium'] : "";
$utm_campaign = $_POST['utm_campaign'] ? $_POST['utm_campaign'] : "";
$utm_term = $_POST['utm_term'] ? $_POST['utm_term'] : "";
$utm_content = $_POST['utm_content'] ? $_POST['utm_content'] : "";
$pais = $_POST['pais'] ? $_POST['pais'] : "";


$data = array(
    'nombre' => $nombre,
    'email' => $email,
    'fechanacimiento' => $fechanacimiento,
    'telefono' => $telefono,
    'marca_auto' => $marca_auto,
    'modelo_auto' => $modelo_auto,
    'año' => $año,
    'localidad' => $localidad,
    'utm_source' => $utm_source,
    'utm_medium' => $utm_medium,
    'utm_campaign' => $utm_campaign,
    'utm_term' => $utm_term,
    'utm_content' => $utm_content,
    'pais' => $pais,
);

$options = array(
    'http' => array(
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query($data)
    )
);
$context  = stream_context_create($options);
$result = file_get_contents($url, false, $context);
if ($result === FALSE) { }