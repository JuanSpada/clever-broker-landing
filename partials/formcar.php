<form id="contact-form1" name="contact-form" method="POST" action="contact.php">

<input type="text" hidden name="formtype" value="form-car">
    <ul>
        <li>
            <label>Nombre y Apellido:</label>
            <div class="automobile-banner-submit">
                <input type="text" name="nombre" required>
            </div>
        </li>
        <li>
            <label>Email:</label>
            <div class="automobile-banner-submit">
                <input type="text" name="email" required>
            </div>
        </li>
        <li>
            <label>Localidad:</label>
            <div class="automobile-banner-submit">
                <input type="text" name="localidad" required>
            </div>
        </li>
        <li>
            <label>Edad:</label>
            <div class="automobile-banner-submit">
                <input type="text" name="fechanacimiento" required>
            </div>
        </li>
        <li>
            <label>Teléfono:</label>
            <div class="automobile-banner-submit">
                <input type="text" name="telefono" required>
            </div>
        </li>

        <li>
            <div id="vehicle">
            <label>Marca:</label>
                <div class="form-group">
                    <div class="automobile-banner-select">
                        <select name="marca_auto" class="formmake-car" required>
                            <option value="" selected="selected">Marca</option>
                        </select>
                    </div>
                </div>
            </div>
        </li>
        <li>
            <label>Modelo:</label>
            <div class="form-group">
                <div class="automobile-banner-select">
                    <select name="modelo_auto" class="formmodel-car" required>
                        <option value="" selected="selected">Modelo</option>
                    </select>
                </div>
            </div>
        </li>
    
        <li>
            <label>Año:</label>
            <div class="automobile-banner-select">
                <select required name="año">
                    <option value="" selected disabled>Año</option>
                    <option value="2020">2020</option>
                    <option value="2019">2019</option>
                    <option value="2018">2018</option>
                    <option value="2017">2017</option>
                    <option value="2016">2016</option>
                    <option value="2015">2015</option>
                    <option value="2014">2014</option>
                    <option value="2013">2013</option>
                    <option value="2012">2012</option>
                    <option value="2011">2011</option>
                    <option value="2010">2010</option>
                    <option value="2009">2009</option>
                </select>
            </div>
        </li>

        <li class="banner-full-form">
            <label class="banner-submit"><i class="fas fa-search"></i><input type="submit" value="COTIZAR AHORA"></label>
        </li>
    </ul>
    <!-- Hidden UTM Fields -->
    <input type="hidden" name="utm_source" value="<?= (isset($_GET['utm_source'])) ? $_GET['utm_source'] : '' ?>">
    <input type="hidden" name="utm_medium" value="<?= (isset($_GET['utm_medium'])) ? $_GET['utm_medium'] : '' ?>">
    <input type="hidden" name="utm_campaign" value="<?= (isset($_GET['utm_campaign'])) ? $_GET['utm_campaign'] : '' ?>">
    <input type="hidden" name="utm_term" value="<?= (isset($_GET['utm_term'])) ? $_GET['utm_term'] : '' ?>">
    <input type="hidden" name="utm_content" value="<?= (isset($_GET['utm_content'])) ? $_GET['utm_content'] : '' ?>">
</form>