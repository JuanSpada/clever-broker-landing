<!--// Footer \\-->
<footer id="automobile-footer" class="automobile-footer-one">

<!--// Footer Widget \\-->
<div class="automobile-footer-widget">
    <div class="container">
        <div class="row d-flex justify-content-between p-1">
            <div class="ssn col-md-6">
                <a href="https://www.argentina.gob.ar/superintendencia-de-seguros">
                    <span>
                        <img src="images/SSN.png" style="width: 150px;">
                    </span>
                </a>
            </div>
            <div class="col-md-6 automobile-copyright py-4 mt-1"> 
                <span>Todos los Derechos Reservados. Clever Broker © Copyright 2019.</p> 
                <p>By <a href="https://www.wemanagement.com.ar">We Management</a></p>
            </div>
        </div>
    </div>
</div>
<!--// Footer Widget \\-->

</footer>
<!--// Footer \\-->