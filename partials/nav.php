<!--// Header \\-->
<header id="automobile-header" class="automobile-header-one">
    <div class="container">
        <div class="row rownav" style="display: flex; justify-content: center;">
            <aside class="col-md-2"> <a href="index.php" class="automobile-logo"><img src="images/logo-clever.png" alt=""></a> </aside>
            <aside class="col-md-8" style="display: flex;">
                <!--// Navigation \\-->
                <nav class="navbar navbar-default">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" id="navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="#">Cotizador</a></li>
                            <li class="active"><a href="#nosotros">Nosotros</a></li>
                            <li class="active"><a href="#companias">Compañias</a></li>
                            <li class="active"><a href="#contacto">Contacto</a></li>
                        </ul>
                    </div>
                </nav>
            </aside>
        </div>
    </div>
</header>
<!--// Header \\-->