<?php
	$utm_campaign = !($_GET['utm_campaign']) ? null : $_GET['utm_campaign'];
	$utm_medium = !($_GET['utm_medium']) ? null : $_GET['utm_medium'];
	$utm_source = !($_GET['utm_source']) ? null : $_GET['utm_source'];
	$utm_content = !($_GET['utm_content']) ? null : $_GET['utm_content'];
	$utm_term = !($_GET['utm_term']) ? null : $_GET['utm_term'];
?>

<form id="contact-form2" name="contact-form" method="POST" action="contact.php">
<input type="text" hidden name="formtype" value="form-bike">

    <ul>
        <li>
            <label>Nombre y Apellido:</label>
            <div class="automobile-banner-submit">
                <input type="text" name="nombre" required>
            </div>
        </li>
        <li>
            <label>Email:</label>
            <div class="automobile-banner-submit">
                <input type="text" name="email" required>
            </div>
        </li>
        <li>
            <label>Edad:</label>
            <div class="automobile-banner-submit">
                <input type="text" name="fechanacimiento" required>
            </div>
        </li>

        <li>
            <label>Teléfono:</label>
            <div class="automobile-banner-submit">
                <input type="text" name="telefono" required>
            </div>
        </li>

        <li>
            <div id="vehicle">
            <label>Marca:</label>
                <div class="form-group">
                    <div class="automobile-banner-select">
                        <select name="make-bike" class="formmake-bike" required>
                            <option value="" selected="selected">Marca</option>
                        </select>
                    </div>
                </div>
            </div>
        </li>
        <li>
            <label>Modelo:</label>
            <div class="form-group">
                <div class="automobile-banner-select" >
                    <select name="model-bike" class="formmodel-bike" required>
                        <option value="" selected="selected">Modelo</option>
                    </select>
                </div>
            </div>
        </li>
        
        <li>
            <label>Año:</label>
            <div class="automobile-banner-select">
                <select required name="año">
                    <option value="" selected disabled>Año</option>
                    <option value="2020">2020</option>
                    <option value="2019">2019</option>
                    <option value="2018">2018</option>
                    <option value="2017">2017</option>
                    <option value="2016">2016</option>
                    <option value="2015">2015</option>
                    <option value="2014">2014</option>
                    <option value="2013">2013</option>
                    <option value="2012">2012</option>
                    <option value="2011">2011</option>
                    <option value="2010">2010</option>
                    <option value="2009">2009</option>
                    <option value="2008">2008</option>
                    <option value="2007">2007</option>
                    <option value="2006">2006</option>
                    <option value="2005">2005</option>
                </select>
            </div>
        </li>
    
        <li>
            <label>Localidad:</label>
            <div class="automobile-banner-select">
                <select name="localidad" required>
                    <option selected disable>Localidad</option>
                    <?php foreach($localidades as $localidad) : ?>
                        <option value="<?=$localidad?>"><?= $localidad ?></option>
                    <?php endforeach ?>
                </select>
            </div>
        </li>
        <li class="banner-full-form">
            <label class="banner-submit"><i class="fas fa-search"></i><input type="submit" value="SOLICITAR COTIZACIÓN"></label>
        </li>
    </ul>
    <!-- Hidden UTM Fields -->
   	<input type="hidden" name="utm_source" value="<?php echo $utm_source; ?>">
   	<input type="hidden" name="utm_medium" value="<?php echo $utm_medium; ?>">
   	<input type="hidden" name="utm_campaign" value="<?php echo $utm_campaign; ?>">
   	<input type="hidden" name="utm_content" value="<?php echo $utm_content; ?>">
	<input type="hidden" name="utm_term" value="<?php echo $utm_term; ?>">
</form>